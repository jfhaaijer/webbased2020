package nl.bioinf.jfhaaijer.SpeciesBrowser.servlets;


import nl.bioinf.jfhaaijer.ImplementingMVC.config.WebConfig;
import nl.bioinf.jfhaaijer.SpeciesBrowser.model.Bird;
import nl.bioinf.jfhaaijer.SpeciesBrowser.model.Role;
import nl.bioinf.jfhaaijer.SpeciesBrowser.model.User;
import nl.bioinf.jfhaaijer.SpeciesBrowser.service.DatabaseTool;
import org.apache.commons.collections4.queue.CircularFifoQueue;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Queue;

import static nl.bioinf.jfhaaijer.SpeciesBrowser.config.WebConfig.birdList;
import static nl.bioinf.jfhaaijer.SpeciesBrowser.service.DatabaseTool.*;

@WebServlet(urlPatterns = "/login")
public class LoginServlet extends HttpServlet {
    private TemplateEngine templateEngine;
    public String username;
    public String user;

    @Override
    public void init() throws ServletException {
        System.out.println("Initializing Thymeleaf template engine");
        final ServletContext servletContext = this.getServletContext();
        WebConfig.createTemplateEngine(servletContext);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.username = request.getParameter("username");
        String password = request.getParameter("password");

        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());

        //fetch the session object
        //if it is not present, one will be created
        HttpSession session = request.getSession();
        String nextPage;
        ctx.setVariable("birds", birdList);


        if (session.isNew() || !user.equals(username)) {
            List<Serializable> userData = VerifyAuthentication(username, password);
            if (userData.get(0) == Boolean.TRUE) {
                this.user = username;
                session.setAttribute(username, new User(username, (Role) userData.get(1)));
                Object obj = session.getAttribute(username);
                nextPage = "species-listing";
                ctx.setVariable("Urole",obj);
            } else {
                ctx.setVariable("message", "Your password and/or username are incorrect; please try again");
                ctx.setVariable("message_type", "error");
                nextPage = "login";
            }
        } else {
            nextPage = "species-listing";

        }
        final ServletContext servletContext = super.getServletContext();
        nl.bioinf.jfhaaijer.ImplementingMVC.config.WebConfig.createTemplateEngine(servletContext).process(nextPage, ctx, response.getWriter());
    }


    //simple GET requests are immediately forwarded to the login page
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());
        ctx.setVariable("message", "Fill out the login form");
        ctx.setVariable("message_type", "info");
        final ServletContext servletContext = super.getServletContext();
        nl.bioinf.jfhaaijer.ImplementingMVC.config.WebConfig.createTemplateEngine(servletContext).process("login", ctx, response.getWriter());

    }
}