package nl.bioinf.jfhaaijer.SpeciesBrowser.model;

public class User {
    private String Username;
    private Role ROLE;

    public User(String username, Role role){
        this.Username = username;
        this.ROLE = setROLE(role);
    }

    public String getUsername() {
        return Username;
    }

    public Role setROLE(Role ROLE) {
        switch(ROLE){
            case ADMIN: return Role.ADMIN;
            case ANON: return Role.ANON;
            case USER: return Role.USER;
        }
        return ROLE;
    }

    public Role getROLE() {
        return ROLE;
    }
}

