package nl.bioinf.jfhaaijer.SpeciesBrowser.model;

import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import nl.bioinf.jfhaaijer.SpeciesBrowser.service.SQLconnector;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

public class ReadDataAndParseToBean {
//    private static final String CSV_FILE_PATH = ;

    public static List<Bird> getBirdObjectsFromFile(String csvfilepath) throws IOException {
        List<Bird> birdsList = new ArrayList<>();
        try (
                Reader reader = Files.newBufferedReader(Paths.get(csvfilepath));
        ) {

            ColumnPositionMappingStrategy strategy = new ColumnPositionMappingStrategy();
            strategy.setType(Bird.class);
            String[] memberFieldsToBindTo = {"scientific_name", "english_name", "dutch_name", "continent", "picture_loc"};
            strategy.setColumnMapping(memberFieldsToBindTo);

            CsvToBean<Bird> csvToBean = new CsvToBeanBuilder(reader)
                    .withMappingStrategy(strategy)
                    .withSkipLines(1)
                    .withIgnoreLeadingWhiteSpace(true)
                    .withSeparator(';')
                    .build();

            birdsList = csvToBean.parse();

        }
        return birdsList;
    }

    public static List<Bird> getBirdObjectsFromDatabase(){
        List<Bird> birdsList = new ArrayList<>();
        Connection conn = null;
        Statement stmt = null;

        try{
            //STEP 2: Register JDBC driver
            Class.forName("com.mysql.cj.jdbc.Driver");

            //STEP 3: Open a connection
            System.out.println("Connecting to a selected database...");
            conn = SQLconnector.initializeDatabase();
            System.out.println("Connected database successfully...");

            //STEP 4: Execute a query
            System.out.println("Verifying password...");
            stmt = conn.createStatement();

            // Get password from table
            stmt.executeQuery("SELECT * FROM birds");

            ResultSet rs = stmt.getResultSet();

            while(rs.next()) {
                Bird birdObject = new Bird(
                        rs.getString("scientific_name"),  // scientific name
                        rs.getString("english_name"),  // english name
                        rs.getString("dutch_name"),  // dutch name
                        rs.getString("continent"),  // continent of origin
                        rs.getString("picture_loc")); // picture path
                birdsList.add(birdObject);
            }
        } catch(Exception se){
            //Handle errors for JDBC
            se.printStackTrace();
        }//Handle errors for Class.forName
        finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                    conn.close();
            }catch(SQLException ignored){
            }// do nothing
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }//end finally try
        }//end try
        return birdsList;
    }//end main,

//
//    public static void main(String args[]){
//        try {
//            List<Bird> birdlist = getBirdObjects();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
}



