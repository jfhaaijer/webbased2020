package nl.bioinf.jfhaaijer.SpeciesBrowser.model;

public enum Role {
    ADMIN, USER, ANON
}
