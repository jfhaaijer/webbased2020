package nl.bioinf.jfhaaijer.SpeciesBrowser.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SQLconnector {

    /**
     * @return the database connection
     * @throws SQLException in case the connection fails
     * @throws ClassNotFoundException in case the class is not found
     */
    public static Connection initializeDatabase()
            throws SQLException, ClassNotFoundException
    {
        // Initialize all the information regarding
        // Database Connection
        String dbDriver = "com.mysql.cj.jdbc.Driver";
        String dbURL = "jdbc:mysql://localhost:3306/WebDatabase?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
        // Database name to access
        String dbUsername = "jfhaaijer" ;
        String dbPassword = "3nGR8f7t";

        Class.forName(dbDriver);
        return DriverManager.getConnection(dbURL,
                dbUsername,
                dbPassword);
    }
}
