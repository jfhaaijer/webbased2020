package nl.bioinf.jfhaaijer.SpeciesBrowser.servlets;

import nl.bioinf.jfhaaijer.ImplementingMVC.config.WebConfig;
import org.apache.commons.collections4.queue.CircularFifoQueue;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Date;
import java.util.Queue;

import static nl.bioinf.jfhaaijer.SpeciesBrowser.config.WebConfig.birdList;


@WebServlet(name = "DetailsServlet", urlPatterns = {"/species.detail"})
public class DetailsServlet extends HttpServlet {
    public static Queue<String> pageHistroy = new CircularFifoQueue<>(5);
    String username;

    @Override
    public void init() throws ServletException {
        System.out.println("Initializing Thymeleaf template engine");
        final ServletContext servletContext = this.getServletContext();
        WebConfig.createTemplateEngine(servletContext);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //this step is optional; standard settings also suffice
        WebConfig.configureResponse(response);
        String birdName = (request.getParameter("birdname"));
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());
        ctx.setVariable("birdname", birdName);
        ctx.setVariable("birds", birdList);
        ctx.setVariable("currentDate", new Date());

        final ServletContext servletContext = super.getServletContext();
        WebConfig.createTemplateEngine(servletContext).process("species.detail", ctx, response.getWriter());
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //this step is optional; standard settings also suffice
        WebConfig.configureResponse(response);

        String birdName = (request.getParameter("species"));
        System.out.print(birdName);
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());

        HttpSession session = request.getSession();

        if (session.isNew()){
            session.setAttribute("history", pageHistroy);
        } else {
            pageHistroy.add(birdName);
            session.setAttribute("history", pageHistroy);
        }
        ctx.setVariable("birds", birdList);
        ctx.setVariable("birdname", birdName);
        ctx.setVariable("currentDate", new Date());
        final ServletContext servletContext = super.getServletContext();
        WebConfig.createTemplateEngine(servletContext).process("species.detail", ctx, response.getWriter());
    }
}
