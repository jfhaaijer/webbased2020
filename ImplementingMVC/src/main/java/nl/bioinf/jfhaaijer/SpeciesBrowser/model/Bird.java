package nl.bioinf.jfhaaijer.SpeciesBrowser.model;

public class Bird {
    private String scientific_name;
    private String english_name;
    private String dutch_name;
    private String continent;
    private String picture_loc;

//    public Bird(String scientific_name,String english_name, String dutch_name, String continent, String picture_loc) {
//        this.continent = continent;
//    }
    public Bird () {}

    public Bird(String scientific_name, String english_name, String dutch_name, String continent, String picture_loc) {
        System.out.print(picture_loc);
        this.scientific_name = scientific_name;
        this.english_name = english_name;
        this.dutch_name = dutch_name;
        this.continent = continent;
        this.picture_loc = picture_loc;
    }
    public String getEnglishName() {
        return english_name;
    }

    public String getDutchName() {
        return dutch_name;
    }

    public String getContinent() {
        return continent;
    }

    public String getPictureLoc() {
        return picture_loc;
    }

    public String getScientific_name() {
        return scientific_name;
    }
}
