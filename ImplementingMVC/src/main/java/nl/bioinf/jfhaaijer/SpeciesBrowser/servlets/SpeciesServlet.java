package nl.bioinf.jfhaaijer.SpeciesBrowser.servlets;

import nl.bioinf.jfhaaijer.ImplementingMVC.config.WebConfig;
import nl.bioinf.jfhaaijer.SpeciesBrowser.model.Bird;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import static nl.bioinf.jfhaaijer.SpeciesBrowser.config.WebConfig.birdList;
import static nl.bioinf.jfhaaijer.SpeciesBrowser.servlets.DetailsServlet.pageHistroy;


@WebServlet(name = "SpecieServlet", urlPatterns = "/species_browser", loadOnStartup = 1)
public class SpeciesServlet extends HttpServlet {

    @Override
    public void init() throws ServletException {
        System.out.println("Initializing Thymeleaf template engine");
        final ServletContext servletContext = this.getServletContext();
        WebConfig.createTemplateEngine(servletContext);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //this step is optional; standard settings also suffice
        WebConfig.configureResponse(response);
        String birdName = (request.getParameter("birdname"));

        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());

        ctx.setVariable("birdname", birdName);
        ctx.setVariable("birds", birdList);
        ctx.setVariable("birds", pageHistroy);


        ctx.setVariable("currentDate", new Date());
        final ServletContext servletContext = super.getServletContext();
        WebConfig.createTemplateEngine(servletContext).process("species_listing", ctx, response.getWriter());
    }
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{
        //this step is optional; standard settings also suffice
        WebConfig.configureResponse(response);

        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());

        ctx.setVariable("birds", birdList);
        ctx.setVariable("birds", pageHistroy);
        ctx.setVariable("currentDate", new Date());
        final ServletContext servletContext = super.getServletContext();
        WebConfig.createTemplateEngine(servletContext).process("species_browser", ctx, response.getWriter());    }

}