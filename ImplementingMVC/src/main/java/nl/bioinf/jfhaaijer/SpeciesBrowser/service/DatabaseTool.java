package nl.bioinf.jfhaaijer.SpeciesBrowser.service;

import nl.bioinf.jfhaaijer.SpeciesBrowser.model.Role;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.List;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

public class DatabaseTool {

    public static List<Serializable> VerifyAuthentication(String username, String password) {
        Connection conn = null;
        Statement stmt = null;

        try{
            //STEP 2: Register JDBC driver
            Class.forName("com.mysql.cj.jdbc.Driver");

            //STEP 3: Open a connection
            System.out.println("Connecting to a selected database...");
            conn = SQLconnector.initializeDatabase();
            System.out.println("Connected database successfully...");

            //STEP 4: Execute a query
            System.out.println("Verifying password...");
            stmt = conn.createStatement();

            // Get password from table
            stmt.executeQuery("SELECT password, role FROM users WHERE username = ('"+username+"')");

            System.out.print("hep");
            ResultSet rs = stmt.getResultSet();
            String ww = null;
            Role role = Role.USER;
            while (rs.next()) {
                ww = rs.getString("password");
                role = Role.valueOf(rs.getString("role"));
            }
            if (ww != null || ww.equals(password)){
                System.out.print("Password verified...");
                return Arrays.asList(TRUE, role);
            } else {
                System.out.print("Wrong password. Try again");
                return Arrays.asList(FALSE);
            }

        } catch(Exception se){
            //Handle errors for JDBC
            se.printStackTrace();
        }//Handle errors for Class.forName
        finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                    conn.close();
            }catch(SQLException ignored){
            }// do nothing
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }//end finally try
        }//end try
        return null;
    }//end main,

    public static void InsertFile(String csvfile) {
        Connection conn = null;
        Statement stmt = null;
        try{
            //STEP 2: Register JDBC driver
            Class.forName("com.mysql.cj.jdbc.Driver");

            //STEP 3: Open a connection
            System.out.println("Connecting to a selected database...");
            conn = SQLconnector.initializeDatabase();
            System.out.println("Connected database successfully...");

            //STEP 4: Execute a query
            System.out.println("Inserting records into the table...");
            stmt = conn.createStatement();

            // Create table if it does not exist
//            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS salmonellosis.Farms (\n" +
//                    "                                     FarmID VARCHAR(5),\n" +
//                    "                                     Latitude VARCHAR(20),\n" +
//                    "                                     Longitude VARCHAR(20),\n" +
//                    "                                     IDate VARCHAR(50),\n" +
//                    "                                     Contamination CHAR(1)\n" +
//                    ");");
            stmt.execute("SET GLOBAL local_infile = 1;");

            // Load csvfile into database
            stmt.executeUpdate(" LOAD DATA LOCAL INFILE '" + csvfile +
                    "' INTO TABLE birds " +
                    " FIELDS TERMINATED BY \';\'" +
                    " LINES TERMINATED BY \'\\n\'" +
                    " IGNORE 1 LINES; ");


            System.out.println("Inserted records into the table...");

        } catch(Exception se){
            //Handle errors for JDBC
            se.printStackTrace();
        }//Handle errors for Class.forName
        finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                    conn.close();
            }catch(SQLException ignored){
            }// do nothing
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }//end finally try
        }//end try
    }
//
//    public static void main(String[] args) {
//        System.out.print("YAAGFDHFGHFHFFHHFF");
//    }
}

