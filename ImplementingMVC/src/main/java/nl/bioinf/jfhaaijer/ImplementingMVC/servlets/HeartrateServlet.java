package nl.bioinf.jfhaaijer.ImplementingMVC.servlets;

import nl.bioinf.jfhaaijer.ImplementingMVC.model.*;
import nl.bioinf.jfhaaijer.ImplementingMVC.config.*;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.Map;

@WebServlet(name = "HeartrateServlet", urlPatterns = "/heart_rate_zones", loadOnStartup = 1)
public class HeartrateServlet extends HttpServlet {
    @Override
    public void init() throws ServletException {
        System.out.println("Initializing Thymeleaf template engine");
        final ServletContext servletContext = this.getServletContext();
        WebConfig.createTemplateEngine(servletContext);
    }
    private static final long serialVersionUID = 1L;
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //this step is optional; standard settings also suffice
        WebConfig.configureResponse(response);
        String maxheartrate = (request.getParameter("mrate"));
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());
        final Map<String, String> zoneDict = HeartZoneCalculator.getZone(maxheartrate);
        ctx.setVariable("zone1", zoneDict.get("zone1"));
        ctx.setVariable("zone2", zoneDict.get("zone2"));
        ctx.setVariable("zone3", zoneDict.get("zone3"));
        ctx.setVariable("zone4", zoneDict.get("zone4"));
        ctx.setVariable("zone5", zoneDict.get("zone5"));
        ctx.setVariable("currentDate", new Date());
        final ServletContext servletContext = super.getServletContext();
        WebConfig.createTemplateEngine(servletContext).process("return_heart_zones", ctx, response.getWriter());
    }
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{
        //this step is optional; standard settings also suffice
        WebConfig.configureResponse(response);
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());
        ctx.setVariable("currentDate", new Date());
        final ServletContext servletContext = super.getServletContext();
        WebConfig.createTemplateEngine(servletContext).process("heart_rate_zones", ctx, response.getWriter());    }

}