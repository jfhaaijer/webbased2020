package nl.bioinf.jfhaaijer.ImplementingMVC.model;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.lang.Integer.parseInt;

public class HeartZoneCalculator {

    public static Map<String, String> getZone(String maxheartrate) {
        List<Integer> percentageslist = Arrays.asList(50, 60, 70, 80, 90, 100);
        Map<String, String> zonedict = new HashMap<String, String>();
        for (int i = 0; i<percentageslist.size(); i++) {
            if (percentageslist.get(i) == 100){
                break;
            }
            String zoneNum = String.format("%s%d", "zone", i+1);
            float a = (float) ((Integer.parseInt(maxheartrate)/100.0) * percentageslist.get(i));
            float b = (float) ((Integer.parseInt(maxheartrate)/100.0) * percentageslist.get(i+1));
            String zoneFormat = String.format("%f - %f", a, b);
            zonedict.put(zoneNum, zoneFormat);
        }
        return zonedict;
    }
}
